import React, {useState} from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import ListItem from '../components/ListItem';
import ListItemSeparator from '../components/ListItemSeparator';
import ListItemDeleteItem from '../components/ListItemDeleteItem';
import Screen from '../components/Screen';

const initialMessages = [
    {
        id: 1,
        title: "T1",
        description: "D1",
        image: require("../assets/mosh.jpg")
    },
    {
        id: 2,
        title: "T2",
        description: "D2",
        image: require("../assets/mosh.jpg")
    },
    {
        id: 3,
        title: "T3",
        description: "D3",
        image: require("../assets/mosh.jpg")
    },
    {
        id: 4,
        title: "T4",
        description: "D4",
        image: require("../assets/mosh.jpg")
    },
]

function MessagesScreen(props) {
    const [messages, setMessages] = useState(initialMessages);
    const [refreshing, setRefresh] = useState(false);

    const handleDelete = message => {
        setMessages(messages.filter(m => m.id !== message.id));
    }
    return (
        <Screen>
            <FlatList data={messages}
            keyExtractor={message => message.id.toString()}
            renderItem={({item}) => 
                <ListItem 
                    title = {item.title}
                    subTitle = {item.subTitle}
                    image = {item.image}
                    onPress = {() => console.log(item)}
                    renderRightActions={() => 
                    <ListItemDeleteItem onPress={() => handleDelete(item)} />
                    }

                    />}
            ItemSeparatorComponent = {ListItemSeparator}
            refreshing={refreshing}
            onRefresh={() => setMessages([
                {
                    id: 4,
                    title: "T4",
                    description: "D4",
                    image: require("../assets/mosh.jpg")
                }
            ])}
            />
        </Screen>
    );
}

const styles = StyleSheet.create({

})

export default MessagesScreen;