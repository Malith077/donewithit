import React, { useEffect, useState } from 'react';
import { FlatList, Platform, StyleSheet, Text } from 'react-native';
import Card from '../components/Card';

import ActivityIndicator from '../components/ActivityIndicator';
import routes from '../navigation/routes';
import listingsApi from '../api/listings';
import Screen from '../components/Screen';
import colours from '../config/colours';
import AppButton from '../components/AppButton';
import useApi from '../hooks/useApi';

function ListingsScreen({navigation}) {
    const { data : listings, error, loading, request : loadListings } = useApi(listingsApi.getListings);
    const [refreshing, setRefreshing] = useState(false);

    const refresh = () =>{
        setRefreshing(true);
        loadListings().then(() => setRefreshing(false));
    }
    useEffect(() => {
        loadListings();
    }, [])

    return (
        <Screen style={styles.screen}>
            {error && 
                <>
                    <Text>Something went wrong</Text> 
                    <AppButton title="Try again" onPress={loadListings} />
                </> 
            }
            <ActivityIndicator visible={loading} size="large" />

            <FlatList 
                data={listings}
                keyExtractor={listing => listing.id.toString()}
                renderItem={
                    ({item}) =>
                    <Card 
                        title={item.title}
                        subtitle={"$" + item.price}
                        imageUrl={item.images[0].url}
                        onPress={() => navigation.navigate(routes.LISTING_DETAILS, item)}
                        thumbnailUrl={item.images[0].thumbnailUrl}
                    />
                }
                onRefresh={refresh}
                refreshing={refreshing}
            />        
        </Screen>
    );
}

const styles = StyleSheet.create({
    screen:{
        padding: Platform.OS ==="android" ? 10 : 20, 
        backgroundColor: colours.light
    }
})

export default ListingsScreen;