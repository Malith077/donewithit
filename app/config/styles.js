import { Platfrom } from 'react-native';

import colours from './colours';

export default {
    colours,
    text:{
        fontSize: 20,
        fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
        color: colours.dark
    }
};