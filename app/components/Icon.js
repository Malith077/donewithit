import React from 'react';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { View, StyleSheet } from 'react-native';
import colours from '../config/colours';

function Icon({name, size=40, backgroundColor=colours.black, iconColour=colours.white}) {
    return (
        <View style={{
            width: size,
            height: size,
            borderRadius:size/2,
            backgroundColor,
            justifyContent: "center",
            alignItems: "center"
        }}>
            <MaterialCommunityIcons name={name} color={iconColour} size={size * 0.5} />
        </View>
    );
}

const styles = StyleSheet.create({
    Icon:{
        

    }
})

export default Icon;