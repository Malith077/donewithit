import React from 'react';
import { View, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import colours from '../config/colours';
import AppText from './AppText';
import { Image } from 'react-native-expo-image-cache';
function Card({title, subtitle, imageUrl, onPress, thumbnailUrl}) {
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={styles.card}>
                <Image style={styles.image} preview={{ uri : thumbnailUrl }} tint="light" uri={imageUrl}/>
                <View style={styles.detailsContainer}>  
                    <AppText style={styles.title} numberOfLines={1}>{title}</AppText>
                    <AppText style={styles.subTitle} numberOfLines={3}>{subtitle}</AppText>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    card:{
        borderRadius: 15,
        backgroundColor: colours.white,
        marginBottom: 20,
        overflow: "hidden"
    },
    detailsContainer: {
        padding: 20
    },
    image:{
        width: "100%",
        height: 200
    },
    subTitle:{
        color: colours.secondaryColour,
        fontWeight: "bold",
    }, 
    title:{
        marginBottom: 7
    }
})

export default Card;