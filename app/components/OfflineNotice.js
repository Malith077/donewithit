import React from 'react';
import { View, StyleSheet } from 'react-native';
import Constants from 'expo-constants';
import colours from '../config/colours';
import { Text } from 'react-native';
import { useNetInfo } from '@react-native-community/netinfo';

function OfflineNotice() {
    const netinfo = useNetInfo();
    console.log(netinfo);
    if(netinfo.type !== "unknown" && netinfo.isInternetReachable === false){     
        return (
            <View style={styles.container} >
                <Text style={styles.text}>No Internet Connection</Text>
            </View>
        );
    }

    return null;
}

const styles = StyleSheet.create({
    container:{ 
        alignItems: "center",
        justifyContent: "center",
        backgroundColor : colours.primaryColour,
        height: 50,
        top: Constants.statusBarHeight,
        width: "100%",
        position: "absolute",
        zIndex: 1,
    },
    text:{
        color: colours.white,
    }
})

export default OfflineNotice;