import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons'

import colours from '../config/colours';

function NewListingButton({onPress}) {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.container}> 
                <MaterialCommunityIcons name='plus-circle' color={colours.white} size={40} />
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container:{ 
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colours.primaryColour,
        borderRadius: 40,
        borderColor: colours.white,
        borderWidth: 10,
        height: 80,
        width: 80,
        bottom: 20
    }
})

export default NewListingButton;