import React from "react";
import { Button, Text } from 'react-native'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer, useNavigation } from '@react-navigation/native'
import Screen from "./app/components/Screen";
import { MaterialCommunityIcons } from '@expo/vector-icons'
import AuthNavigator from "./app/navigation/AuthNavigator";
import NavigationTheme from "./app/navigation/navigationTheme";
import AppNavigator from "./app/navigation/AppNavigator";
import AsyncStorage from '@react-native-async-storage/async-storage';
import OfflineNotice from "./app/components/OfflineNotice";


export default function App(){
  return(
    <>
      <OfflineNotice />
      <NavigationContainer theme={NavigationTheme}>
        <AppNavigator />
      </NavigationContainer>
    </>
  );
}